# README #

The project contains all files required to work.

### What is this repository for? ###

A simple javascript project for nona.

### How do I get set up? ###

Run the program:  
1. Open web/index.html in the browser of your choice.  
2. Upload the csv file of your choice.  
3. Inspect the results shown. Inspect errors that might have happened in the console.  
4. To upload a new file refresh the page and follow from step 1.  

Run Tests:  
To run tests go to js/SpecRunner.html and open it in the browser of your choice. Tests where written in jasmine.

### Who do I talk to? ###

Ian Cawood

### Important decisions ###

Libraries used:  
1. JQuery to display the data and upload the csv file.  
2. PapaParse to parse the csv file.  

Classes:  
1. FootballTeam is a class that defines a single football team.

Methods:  
1. readCSVFile takes a csv file as input and parses the csv file using the papaParse library. Rows are parsed indivually.  
2. parseRow parses the individual rows that are obtained from papaParse. The rules are defined in js/spec/NonaSoccerSpec.  
3. processMatchResult processes the current rows match result.  
4. findWithAttr finds the index of an object in an array of objects by one the objects attributes.  
5. sortAndDetermineLeaguePosition sorts the football teams by points and determines the league position.

Reason for choosing PapaParse:  
1. It is fault tolerant.  
2. It is fast.  
3. It can handle large files.  
4. It has tests.  

Reason for choosing Jasmine as a testing framework:  
1. Easy to setup.  
2. Learning curve to start testing is low.  
3. It is lightweight.  