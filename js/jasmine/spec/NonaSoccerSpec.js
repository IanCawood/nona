describe("parse individual row", function() {
    it("should expect error for data not equal to 4", function() {
        var row = ["Man", "1", "2", "City", "p"];

        var result = parseRow(row);

        expect(result.error).not.toBe(null);
        expect(result.success).toBe(false);
        expect(result.error).toEqual("Unknown length of data detected");
    });

    it("should expect error for non numbers for score", function() {
        var row = ["Man", "f", "City", "1"];

        var result = parseRow(row);

        expect(result.error).not.toBe(null);
        expect(result.success).toBe(false);
        expect(result.error).toEqual("Number cannot be parsed");
    });

    it("team names should not be empty", function() {
        var row = ["", "1", "Asdf", "2"];

        var result = parseRow(row);

        expect(result.error).not.toBe(null);
        expect(result.success).toBe(false);
        expect(result.error).toEqual("Team name cannot be empty");
    });

    it("correct data should return success", function() {
        var row = ["Man", "2", "City", "2"];

        var result = parseRow(row);

        expect(result.error).toBe(null);
        expect(result.success).toBe(true);
    });
});