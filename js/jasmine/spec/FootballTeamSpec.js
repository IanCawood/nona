describe("FootballTeam", function() {
    var footballTeam;
    var winPoints = 3;
    var drawPoints = 1;
    var lossPoints = 0;

    beforeEach(function() {
        footballTeam = new FootballTeam("Man United");
    });

    it("should create a team", function() {
        var expectedTeam = new FootballTeam("Man United");

        expect(footballTeam).toEqual(expectedTeam);
    });

    it("should record a win", function() {
        var expectedTeam = new FootballTeam("Man United");

        expectedTeam.totalMatches++;
        expectedTeam.matchesWon++;
        expectedTeam.points = expectedTeam.points + winPoints;

        footballTeam.matchWon();

        expect(footballTeam).toEqual(expectedTeam);
    });

    it("should record a loss", function() {
        var expectedTeam = new FootballTeam("Man United");

        expectedTeam.totalMatches++;
        expectedTeam.matchesLost++;
        expectedTeam.points = expectedTeam.points + lossPoints;

        footballTeam.matchLost();

        expect(footballTeam).toEqual(expectedTeam);
    });

    it("should record a draw", function() {
        var expectedTeam = new FootballTeam("Man United");

        expectedTeam.totalMatches++;
        expectedTeam.matchesDrawn++;
        expectedTeam.points = expectedTeam.points + drawPoints;

        footballTeam.matchDrawn();

        expect(footballTeam).toEqual(expectedTeam);
    });

    it("should record a win,loss and a draw", function() {
        var expectedTeam = new FootballTeam("Man United");

        expectedTeam.totalMatches = expectedTeam.totalMatches + 3;
        expectedTeam.matchesDrawn++;
        expectedTeam.matchesWon++;
        expectedTeam.matchesLost++;
        expectedTeam.points = expectedTeam.points + winPoints + drawPoints + lossPoints;

        footballTeam.matchDrawn();
        footballTeam.matchLost();
        footballTeam.matchWon();

        expect(footballTeam).toEqual(expectedTeam);
    });
});