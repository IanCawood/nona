var footballTeams = [];

var readCSVFile = function(csvFileName) {
    Papa.parse(csvFileName, {
        step: function(row) {
            //this step parses row by row
            var result = parseRow(row.data[0]);

            //Will log the results of all falied rows with the data of the row underneath it
            if (!result.success) {
                console.log(result);
                console.log(row);
            }
        },
        complete: function() {
            //This function runs when the file is done loading
            sortAndDetermineLeaguePosition();

            var table = $("#resultsTable");

            for (var i = 0; i < footballTeams.length; i++) {
                table.append("<tr><td>" + footballTeams[i].leaguePosition + "</td><td>" + footballTeams[i].name + "</td><td>" + footballTeams[i].totalMatches + "</td><td>" + footballTeams[i].matchesWon + "</td><td>" + footballTeams[i].matchesLost + "</td><td>" + footballTeams[i].matchesDrawn + "</td><td>" + footballTeams[i].points + "</tr>");
            }

            $("#tableDiv").show();
        }
    });
};

function sortAndDetermineLeaguePosition() {
    //Normal sort
    footballTeams.sort(function(a, b) {
        return b.points - a.points;
    });

    footballTeams[0].leaguePosition = 1;

    //This determines if teams should have the same league position based on points. There is no proper way to break ties without having more data.
    for (var i = 1; i < footballTeams.length; i++) {
        if (footballTeams[i].points === footballTeams[i - 1].points) {
            footballTeams[i].leaguePosition = footballTeams[i - 1].leaguePosition;
        } else {
            footballTeams[i].leaguePosition = i + 1;
        }
    }
}

function findWithAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }

    return -1;
}

var processMatchResult = function(teamOne, teamTwo, scores) {
    if (scores[0] > scores[1]) {
        teamOne.matchWon();
        teamTwo.matchLost();
    } else if (scores[0] < scores[1]) {
        teamOne.matchLost();
        teamTwo.matchWon();
    } else if (scores[0] === scores[1]) {
        teamOne.matchDrawn();
        teamTwo.matchDrawn();
    }
};

var parseRow = function(row) {
    var result = {
        success: false,
        error: null
    };

    //Check if data is in a valid format
    if (row.length != 4) {
        result.error = "Unknown length of data detected";
        return result;
    }

    var scores = [parseInt(row[1]), parseInt(row[3])];

    //Check if scores are in a valid format
    if (isNaN(scores[0]) || isNaN(scores[1])) {
        result.error = "Number cannot be parsed";
        return result;
    }

    //Check if team names are defined
    if (row[0] == "" || row[2] == "") {
        result.error = "Team name cannot be empty";
        return result;
    }

    var teamOneIndex = findWithAttr(footballTeams, 'name', row[0]);

    //If team does not exist create new and push it to the array
    if (teamOneIndex === -1) {
        footballTeams.push(new FootballTeam(row[0]));
        teamOneIndex = footballTeams.length - 1;
    }

    var teamTwoIndex = findWithAttr(footballTeams, 'name', row[2]);

    //If team does not exist create new and push it to the array
    if (teamTwoIndex === -1) {
        footballTeams.push(new FootballTeam(row[2]));
        teamTwoIndex = footballTeams.length - 1;
    }

    result.success = true;

    processMatchResult(footballTeams[teamOneIndex], footballTeams[teamTwoIndex], scores);

    return result;
};

$(document).ready(function() {
    $("#tableDiv").hide();

    $("#uploadFile").on("click", function() {
        $('#csvDiv').hide();
        var file = $("#csvFile")[0].files[0];

        if (!file) {
            alert("No File selected");
            return;
        }

        readCSVFile(file);
    });
});