var FootballTeam = class FootballTeam {
    constructor(name) {
        this.name = name;
        this.totalMatches = 0;
        this.matchesWon = 0;
        this.matchesDrawn = 0;
        this.matchesLost = 0;
        this.points = 0;
    }

    matchWon() {
        this.totalMatches++;
        this.matchesWon++;
        this.points = this.points + 3;
    }

    matchLost() {
        this.totalMatches++;
        this.matchesLost++;
    }

    matchDrawn() {
        this.totalMatches++;
        this.matchesDrawn++;
        this.points++;
    }
}